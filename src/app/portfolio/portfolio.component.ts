import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {PortfolioGalleryComponent} from '../portfolio-gallery/portfolio-gallery.component';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {fade, slide} from '../animations';


declare function alertMe(): any;

export interface DialogData {
  id: number;
}

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css'],
  animations: [
    fade,
    slide
  ]
})
export class PortfolioComponent implements OnInit {

  imagePath = [
    {id: 1, path: '../../assets/images/graphics/bike.png', name: 'Bike'},
    {id: 2, path: '../../assets/images/graphics/llama.png',  name: 'Llama'},
    {id: 3, path: '../../assets/images/graphics/tree.png',  name: 'Tree'},
    {id: 4, path: '../../assets/images/graphics/cake.png',  name: 'Cake'},
    {id: 5, path: '../../assets/images/graphics/therapistIcons.png',  name: 'Icons'},
    {id: 6, path: '../../assets/images/graphics/abcLogo.png',  name: 'ABC Logo'},
    {id: 7, path: '../../assets/images/graphics/catalistLogo.png',  name: 'Catalist Logo'},
    {id: 8, path:  '../../assets/images/graphics/macroLogo.png',  name: 'Macro Logo'},
    {id: 9, path: '../../assets/images/graphics/colorLogo.png',  name: 'Color Logo'},
  ];

  show = true;


  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
      this.show = !this.show;
      setTimeout(() => {
        this.show = !this.show;
      }, 0);

    // alertMe();
  }


  open(imageId: number): void {
    const dialogRef = this.dialog.open(PortfolioGalleryComponent, {
      data: {id: imageId}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('dialog closed');
    });
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView({block: 'start', behavior: 'smooth'});
  }

  showAnimation() {
    this.show = !this.show;
    setTimeout(() => {
      this.show = !this.show;
    }, 0);
  }


}
