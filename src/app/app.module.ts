import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {MatTooltipModule} from '@angular/material/tooltip';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { PortfolioGalleryComponent } from './portfolio-gallery/portfolio-gallery.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {InViewportModule} from 'ng-in-viewport';
import { SvgAnimationComponent } from './svg-animation/svg-animation.component';

@NgModule({
  declarations: [
    AppComponent,
    PortfolioComponent,
    PortfolioGalleryComponent,
    SvgAnimationComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: AppComponent},
      {path: '**', component: AppComponent }
      ]),
    MatDialogModule,
    MatButtonModule,
    MatRippleModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    InViewportModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
