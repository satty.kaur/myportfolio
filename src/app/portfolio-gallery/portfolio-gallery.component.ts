import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../portfolio/portfolio.component';


@Component({
  selector: 'app-portfolio-gallery',
  templateUrl: './portfolio-gallery.component.html',
  styleUrls: ['./portfolio-gallery.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PortfolioGalleryComponent implements OnInit {

  id;

  constructor(
    public dialogRef: MatDialogRef<PortfolioGalleryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit(): void {
    this.id = this.data.id;
    console.log(this.id);
  }





}
