import {animate, query, stagger, style, transition, trigger} from '@angular/animations';


export let fade =  trigger('fade', [
  transition('void => *', [
    query(':enter', [
      style({opacity: 0}),
      stagger(200, [
        animate(300,
          style({opacity: 1}))
      ])
    ])
])
]);

export let slide = trigger('slide', [
  transition('void => *', [
    style({transform: 'translateX(100vw)'}),
    animate(500)
  ])
  ]
);
