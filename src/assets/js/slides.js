
function loadSlides() {
  $('.slides').slick({
    infinite: true,
    slidesToShow: 1,
    dots: true,
    prevArrow: false,
    nextArrow: false,
  });
}
