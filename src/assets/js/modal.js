

function openModal() {

  let modal = document.getElementById("modal");
  let img = document.getElementById("myImg");
  let modalImg = document.getElementById("img01");
  let caption = document.getElementById("caption");

  img.onclick = function () {
    modal.style.display = "block";
    modalImg.src = "";
    caption.innerHTML = this.alt;
  };

  let span = document.getElementsByClassName("close")[0];

  span.onclick = function () {
    modal.style.display = "none";
  };
}

function alertMe() {
  alert("Hello there")
}

